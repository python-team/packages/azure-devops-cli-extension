azure-devops-cli-extension (1.0.1-1) unstable; urgency=medium

  * Merge remote-tracking branch 'origin/release-1.0.1' into debian/sid
  * d/control: bump Standards-Version to 4.7.0, no changes
  * Ignore new failing test
  * Bump dependency on azure-cli

 -- Luca Boccassi <bluca@debian.org>  Thu, 23 May 2024 20:54:08 +0100

azure-devops-cli-extension (0.26.0-1) unstable; urgency=medium

  * Merge remote-tracking branch 'origin/Releases-0.26.0' into debian/sid
  * Bump Standards-Version to 4.6.2, no changes
  * Bump copyright year ranges in d/copyright
  * Skip new broken test

 -- Luca Boccassi <bluca@debian.org>  Tue, 31 Jan 2023 18:48:36 +0000

azure-devops-cli-extension (0.25.0-1) unstable; urgency=medium

  * Merge remote-tracking branch 'origin/release-0.25.0' into debian/sid
  * Bump Standards-Version to 4.6.1, no changes

 -- Luca Boccassi <bluca@debian.org>  Tue, 13 Dec 2022 10:48:04 +0000

azure-devops-cli-extension (0.24.0-1) unstable; urgency=medium

  * Merge tag '20220310.1' into debian/sid

 -- Luca Boccassi <bluca@debian.org>  Mon, 14 Mar 2022 12:36:36 +0000

azure-devops-cli-extension (0.23.0-1) unstable; urgency=medium

  * Merge tag '20220131.1' into debian/sid
  * Bump copyright year ranges in d/copyright

 -- Luca Boccassi <bluca@debian.org>  Tue, 08 Feb 2022 14:41:37 +0000

azure-devops-cli-extension (0.22.0-2) unstable; urgency=medium

  * Add build-dependency on python3-azure-devtools, required by tests
  * Use d/clean instead of PYBUILD_AFTER_CLEAN

 -- Luca Boccassi <bluca@debian.org>  Tue, 11 Jan 2022 13:18:06 +0000

azure-devops-cli-extension (0.22.0-1) unstable; urgency=medium

  * Add dependency on colorama
  * Merge remote-tracking branch 'origin/release-0.22.0' into debian/sid
  * Require azure-cli 2.30 or greater, due to incompatible change
  * d/copyright: bump year range

 -- Luca Boccassi <bluca@debian.org>  Wed, 17 Nov 2021 19:19:33 +0000

azure-devops-cli-extension (0.21.0-1) unstable; urgency=medium

  * Merge tag '20211028.1' into debian/sid
  * Bump debhelper-compat to 13
  * Bump Standards-Version to 4.6.0, no changes
  * Switch to dh-sequence-python3
  * autopkgtest: add dependency on python3-all:any
  * Skip failing tests
  * Ignore very-long-line-length-in-source-file lintian warning

 -- Luca Boccassi <bluca@debian.org>  Fri, 05 Nov 2021 21:51:30 +0000

azure-devops-cli-extension (0.19.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Luca Boccassi ]
  * Merge tag '20210104.2' into debian/sid
  * Bump Standards-Version to 4.5.1, no changes

 -- Luca Boccassi <bluca@debian.org>  Tue, 05 Jan 2021 14:44:18 +0000

azure-devops-cli-extension (0.18.0-2) unstable; urgency=medium

  * Add sys_path_none.patch to fix FTBFS (Closes: #963407)
  * Remove redundant autopkgtest from Testsuite field in d/control

 -- Luca Boccassi <bluca@debian.org>  Sat, 25 Jul 2020 16:30:59 +0100

azure-devops-cli-extension (0.18.0-1) unstable; urgency=medium

  * Update to new upstream release 0.18.0

 -- Luca Boccassi <bluca@debian.org>  Mon, 06 Apr 2020 11:54:24 +0100

azure-devops-cli-extension (0.17.0-5) unstable; urgency=medium

  * Add missing dependency on python3-keyring

 -- Luca Boccassi <bluca@debian.org>  Wed, 25 Mar 2020 10:37:51 +0000

azure-devops-cli-extension (0.17.0-4) unstable; urgency=medium

  * Move package under Python Modules Team maintenance

 -- Luca Boccassi <bluca@debian.org>  Sat, 07 Mar 2020 10:50:14 +0000

azure-devops-cli-extension (0.17.0-3) unstable; urgency=medium

  * Add Testsuite: autopkgtest-pkg-python to d/control
  * Enable tests with pytest, deselect those that need Internet access
  * Add autopkgtest support

 -- Luca Boccassi <bluca@debian.org>  Sun, 01 Mar 2020 16:19:31 +0000

azure-devops-cli-extension (0.17.0-2) unstable; urgency=medium

  * Symlink module and egg-info directories in dist-packages/azure-cli-
    extensions. Allows the extension to be used directly from azure-cli
    without any user setup.

 -- Luca Boccassi <bluca@debian.org>  Wed, 26 Feb 2020 22:51:05 +0000

azure-devops-cli-extension (0.17.0-1) unstable; urgency=medium

  * Initial release. (Closes: #951788)

 -- Luca Boccassi <bluca@debian.org>  Sun, 23 Feb 2020 12:31:36 +0000
